#ifndef HMC5883L_H
#define HMC5883L_H

#include <Arduino.h>
#include <Stream.h>

#include "ISensorIMU.h"
#include "Vector3.h"

namespace rocket{
namespace sensors{
namespace hmc5883l{
    enum Mode: uint8_t{
        MODE_IDLE      = 0b10,
        MODE_SINGLE    = 0b01,
        MODE_CONTINOUS = 0b00
    };
    
    enum Range: uint8_t{
        RANGE_8_1GA  = 0b111,
        RANGE_5_6GA  = 0b110,
        RANGE_4_7GA  = 0b101,
        RANGE_4GA    = 0b100,
        RANGE_2_5GA  = 0b011,
        RANGE_1_9GA  = 0b010,
        RANGE_1_3GA  = 0b001,
        RANGE_0_88GA = 0b000
    };
    
    enum DataRate: uint8_t{
        DATARATE_75HZ    = 0b110,
        DATARATE_30HZ    = 0b101,
        DATARATE_15HZ    = 0b100,
        DATARATE_7_5HZ   = 0b011,
        DATARATE_3HZ     = 0b010,
        DATARATE_1_5HZ   = 0b001,
        DATARATE_0_75_HZ = 0b000
    };
    
    enum SamplesCount: uint8_t{
        SAMPLES_COUNT_8 = 0b11,
        SAMPLES_COUNT_4 = 0b10,
        SAMPLES_COUNT_2 = 0b01,
        SAMPLES_COUNT_1 = 0b00
    };
    
    class HMC5883L: virtual public ISensorIMU{
        public:
        virtual bool initialize(uint8_t mode, uint8_t range, uint8_t dataRate);
        virtual bool selfTest(Stream* logSerial = NULL);
        virtual vector3::Vector3 read();
        
        virtual void setMode(uint8_t mode);
        virtual void setRange(uint8_t range);
        virtual void setDataRate(uint8_t dataRate);
                void setSamplesCount(SamplesCount samplesCount);
                void setBias(double bx, double by, double bz);
                void setCalibrationMatrix(double m00, double m01, double m02, 
                                          double m10, double m11, double m12, 
                                          double m20, double m21, double m22);
        
        virtual uint8_t getMode();
        virtual uint8_t getRange();
        virtual uint8_t getDataRate();
           SamplesCount getSamplesCount();
        
        protected:
        virtual vector3::Vector3 readRaw();
        
        bool selfTestCheckEachRange(bool isPositive, Stream* logSerial = NULL);
    
        const uint8_t addr_       = 0x1E;
        const uint8_t regIdentA_  = 0x0A;
        const uint8_t regIdentB_  = 0x0B;
        const uint8_t regIdentC_  = 0x0C;
        
        const uint8_t regConfigA_ = 0x00;
        const uint8_t regConfigB_ = 0x01;
        const uint8_t regMode_    = 0x02;
        
        const uint8_t regOutXh_   = 0x03;
        
        double gain_;
        
        vector3::Vector3 bias_          =  {0.0f, 0.0f, 0.0f};
        double calibrationMatrix_[3][3] = {{1.0f, 0.0f, 0.0f}, 
                                           {0.0f, 1.0f, 0.0f}, 
                                           {0.0f, 0.0f, 1.0f}};
    };
}
}
}

#endif