#ifndef BMP085_H
#define BMP085_H

#include <Arduino.h>

namespace rocket{
namespace sensors{
namespace bmp085{
    enum Oversampling: uint8_t{
        OVERSAMPLING_ULTRA    = 0x03,
        OVERSAMPLING_HIGH     = 0x02,
        OVERSAMPLING_STANDARD = 0x01,
        OVERSAMPLING_OFF      = 0x00
    };
    
    class BMP085{
        public:
        bool initialize(Oversampling oversampling);
        
        void requestUpdate();
        
        int16_t readAltitude();
        int32_t readPressure();
        int16_t readTemperature();
        
        void setSeaLevelPressure(uint32_t seaLevelPressure);
        
        private:
        void processRawValues(int32_t rawPressure, uint16_t rawTemperature);
        
        int32_t readRawPressure();
        uint16_t readRawTemperature();
        
        const uint8_t addr_       = 0x77; 
        const uint8_t regChipId_  = 0xD0;
        
        const uint8_t regAc1_     = 0xAA;
        //const uint8_t regAc2_     = 0xAC;
        //const uint8_t regAc3_     = 0xAE;
        //const uint8_t regAc4_     = 0xB0;
        //const uint8_t regAc5_     = 0xB2;
        //const uint8_t regAc6_     = 0xB4;
        
        //const uint8_t regB1_      = 0xB6;
        //const uint8_t regB2_      = 0xB8;
        
        //const uint8_t regMb_      = 0xBA;
        //const uint8_t regMc_      = 0xBC;
        //const uint8_t regMd_      = 0xBE;
        
        const uint8_t regCtrl_    = 0xF4;
        const uint8_t regData_    = 0xF6;
        
        //Для калибровки.
         int16_t b1_, b2_;        
         int16_t mb_, mc_, md_;
         int16_t ac1_, ac2_, ac3_;
        uint16_t ac4_, ac5_, ac6_;
        
        Oversampling oversampling_;
        
        uint32_t seaLevelPressure_   = 101325;
        
        
        uint32_t readyDataTime_      = 0;
        
        uint16_t lastRawTemperature_ = 0;
        
        int16_t lastAltitude_        = 0;
        int32_t lastPressure_        = 0;
        int16_t lastTemperature_     = 0;
        
        bool isTemperatureRequested_ = false;
    };
}
}
}

#endif