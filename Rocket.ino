#include <Wire.h>

#include "Rocket.h"

rocket::RocketManager rm;


void setup(){
    Serial.begin(115200);
    
    if(Serial){
        Serial.println("Debug command:\n    -radio    hc-12 flash mode.");
        
        delay(5000);
        
        if(Serial.available()){
            String cmd = Serial.readString();
            
            if(cmd == "-radio"){
                Serial.println("Hey");
            }
        }
        else{
            Serial.println("No commands.");
        }
        
        Serial.println("Running rocket...");
    }
    
    
    
    
    Wire.begin();
    Wire.setClock(400000);

    Serial1.begin(115200);
    
    if(!rm.initialize(&Serial1)){
        while(true) {}
    }
}

void loop(){
    rm.update();
}

