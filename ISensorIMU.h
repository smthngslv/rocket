#ifndef ISENSOR_H
#define ISENSOR_H

#include <Arduino.h>
#include <Stream.h>
#include "Vector3.h"

namespace rocket{
namespace sensors{
    class ISensorIMU{
        public:
            virtual ~ISensorIMU() {}
            
            virtual bool initialize(uint8_t mode, uint8_t range, uint8_t dataRate) = 0;
            virtual bool selfTest(Stream* logSerial = NULL) = 0;
            virtual vector3::Vector3 read() = 0;
            
            virtual void setMode(uint8_t mode) = 0;
            virtual void setRange(uint8_t range) = 0;
            virtual void setDataRate(uint8_t dataRate) = 0;
            
            virtual uint8_t getMode() = 0;
            virtual uint8_t getRange() = 0;
            virtual uint8_t getDataRate() = 0;
        
        protected:
            virtual vector3::Vector3 readRaw() = 0;
            
    };
}
}

#endif