#include "ADXL345.h"

//С этим не компилится.
//#include <Arduino.h>
#include <Stream.h>
#include <avr/pgmspace.h>

#include "Vector3.h"
#include "I2C_utils.h"

namespace rocket{
namespace sensors{
namespace adxl345{
    bool ADXL345::initialize(uint8_t mode, uint8_t range, uint8_t dataRate){
        if(i2c_utils::readRegister8(addr_, regDevId_) != 0xE5){
            return false;
        }
        
        //Включаем.
        i2c_utils::writeRegister8(addr_, regPowerCtrl_, 0x08);
        
        setMode(mode);
        setRange(range);
        setDataRate(dataRate);
        
        return true;        
    }
    
    bool ADXL345::selfTest(Stream* logSerial){
        if(logSerial){ logSerial->println(F("ADXL345: self-test begining...")); }
        
        Mode prevMode = (Mode)getMode();
        Range prevRange = (Range)getRange();
        DataRate prevDataRate = (DataRate)getDataRate();
        
        setMode(MODE_BYPASS);
        setDataRate(DATARATE_800HZ);
        
        bool flag = selfTestCheckEachRange(logSerial);
        
        setMode(prevMode);
        setRange(prevRange);
        setDataRate(prevDataRate);
        
        delay(100);
        
        if(logSerial && flag){ logSerial->println(F("ADXL345: self-test was successful.\n")); }
        
        return flag;
    }
    
    vector3::Vector3 ADXL345::read(){
        vector3::Vector3 norm = readRaw() * gain_;
        
        if(norm.x > 0){
            norm.x *= axesGainCorrection_.x;
        }
        else{
            norm.x *= axesGainNegativeCorrection_.x;
        }
        
        if(norm.y > 0){
            norm.y *= axesGainCorrection_.y;
        }
        else{
            norm.y *= axesGainNegativeCorrection_.y;
        }
        
        if(norm.z > 0){
            norm.z *= axesGainCorrection_.z;
        }
        else{
            norm.z *= axesGainNegativeCorrection_.z;
        }
        
        return  norm;
    }
    
    void ADXL345::setMode(uint8_t mode){
        i2c_utils::writeRegister8(addr_, regFifoCtrl_, 
            (i2c_utils::readRegister8(addr_, regFifoCtrl_) & 0x3F) | ((Mode)mode << 6));
    }
    
    void ADXL345::setRange(uint8_t range){
        static const double gains[4] = {0.0039f, 0.0078f, 0.0156f, 0.0312f};
        
        i2c_utils::writeRegister8(addr_, regDataFormat_, 
            (i2c_utils::readRegister8(addr_, regDataFormat_) & 0xF4) | (Range)range);
        gain_ = gains[(Range)range];
    }
    
    void ADXL345::setDataRate(uint8_t dataRate){
        i2c_utils::writeRegister8(addr_, regBWRate_, 
            (i2c_utils::readRegister8(addr_, regBWRate_) & 0x10) | (DataRate)dataRate);
    }
    
    void ADXL345::setAxesGainCorrection(double cx, double cy, double cz, 
                                        double cxn, double cyn, double czn){
        axesGainCorrection_ = vector3::Vector3(cx, cy, cz);
        axesGainNegativeCorrection_ = vector3::Vector3(cxn, cyn, czn);
    }
    
    uint8_t ADXL345::getMode(){
        return (Mode)((i2c_utils::readRegister8(addr_, regFifoCtrl_) & 0xC0) >> 6);
    }
    
    uint8_t ADXL345::getRange(){
        return (Range)(i2c_utils::readRegister8(addr_, regDataFormat_) & 0x03);
    }
    
    uint8_t ADXL345::getDataRate(){
        return (DataRate)(i2c_utils::readRegister8(addr_, regBWRate_) & 0x0F);
    }
    
    vector3::Vector3 ADXL345::readRaw(){
        uint8_t buff[6];
        
        i2c_utils::readNearRegisters(addr_, regOutXl_, buff, 0, 6);
        
        vector3::Vector3 raw;
        raw.x = (double)(buff[1] << 8 | buff[0]);
        raw.y = (double)(buff[3] << 8 | buff[2]);
        raw.z = (double)(buff[5] << 8 | buff[4]);
        
        return raw;
    }
    
    bool ADXL345::selfTestCheckEachRange(Stream* logSerial = NULL){
        const uint8_t avgSamplesCount = 100;
        
        static const int16_t lowerThresholdXY[4] = { 50,   25, 12,  6};
        static const int16_t upperThresholdXY[4] = {540, 270, 135, 67};
        
        static const int16_t lowerThresholdZ[4] = { 75,  38,  19,  10};
        static const int16_t upperThresholdZ[4] = {875, 438, 219, 100};
        
        for(uint8_t range = RANGE_2G; range <= RANGE_16G; range++){
            if(logSerial){ logSerial->print(F("ADXL345:    range: ")); logSerial->println(range); }
            
            setRange(range);
            delay(100);
            
            vector3::Vector3 externalVals = {0.0f, 0.0f, 0.0f};
            for(uint8_t sampl = 0; sampl < avgSamplesCount; sampl++){
                externalVals += readRaw();
            }
            externalVals /= avgSamplesCount;
            
            //Включаем.
            i2c_utils::writeRegister8(addr_, regDataFormat_, 
                (i2c_utils::readRegister8(addr_, regDataFormat_) & 0x77) | (0x01 << 7));
            delay(100);
            
            vector3::Vector3 val = {0.0f, 0.0f, 0.0f};
            for(uint8_t sampl = 0; sampl < avgSamplesCount; sampl++){
                val += readRaw();
            }
            val /= avgSamplesCount;
            
            //Выключаем.
            i2c_utils::writeRegister8(addr_, regDataFormat_, 
                (i2c_utils::readRegister8(addr_, regDataFormat_) & 0x77));
            
            //Отнимаем.
            val -= externalVals;
            
            //Приводим к одному знаку.
            val.y *= -1;
            
            if(logSerial){ logSerial->print(F("ADXL345:    ")); logSerial->print(val.x);
                                                                logSerial->print(F(" "));
                                                                logSerial->print(val.y);
                                                                logSerial->print(F(" "));
                                                                logSerial->print(val.z);}
            
            
            if(!((val.x > lowerThresholdXY[range] && val.x < upperThresholdXY[range])
               &&(val.y > lowerThresholdXY[range] && val.y < upperThresholdXY[range])
               &&(val.z >  lowerThresholdZ[range] && val.z <  upperThresholdZ[range]))){
                
                if(logSerial){ logSerial->println(F(" ERROR")); }
                
                return false;
            }
            
            if(logSerial){ logSerial->println(F(" OK")); }
        }
        
        return true;
    }
}
}
}