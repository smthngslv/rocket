#include "HMC5883L.h"

#include <Arduino.h>
#include <Stream.h>
#include <avr/pgmspace.h>

#include "Vector3.h"
#include "I2C_utils.h"

namespace rocket{
namespace sensors{
namespace hmc5883l{
    bool HMC5883L::initialize(uint8_t mode, uint8_t range, uint8_t dataRate){
        if((i2c_utils::readRegister8(addr_, regIdentA_) != 0x48)
        || (i2c_utils::readRegister8(addr_, regIdentB_) != 0x34)
        || (i2c_utils::readRegister8(addr_, regIdentC_) != 0x33)){
            return false;
        }
        
        setMode(mode);
        setRange(range);
        setDataRate(dataRate);
        
        return true;
    }
    
    bool HMC5883L::selfTest(Stream* logSerial = NULL){
        if(logSerial){ logSerial->println(F("HMC5883L: self-test begining...")); }
        
        Mode prevMode = getMode();
        Range prevRange = getRange();
        DataRate prevDataRate = getDataRate();
        SamplesCount prevSamplesCount = getSamplesCount();
        
        setMode(MODE_CONTINOUS);
        setDataRate(DATARATE_75HZ);
        setSamplesCount(SAMPLES_COUNT_1);
        
        if(logSerial){ logSerial->println(F("HMC5883L:    positive direction:")); }
        
        //+
        i2c_utils::writeRegister8(addr_, regConfigA_, 
            (i2c_utils::readRegister8(addr_, regConfigA_) & 0x7C) | 0x01);
            
        bool flag = selfTestCheckEachRange(true, logSerial);
        
        if(flag){
            if(logSerial){ logSerial->println(F("HMC5883L:    negative direction:")); }
            
            //-
            i2c_utils::writeRegister8(addr_, regConfigA_, 
                (i2c_utils::readRegister8(addr_, regConfigA_) & 0x7C) | 0x02);
                
            flag = selfTestCheckEachRange(false, logSerial);
        }
        
        //Выключаем.
        i2c_utils::writeRegister8(addr_, regConfigA_, 
            (i2c_utils::readRegister8(addr_, regConfigA_) & 0x7C) | 0x00);
        
        setMode(prevMode);
        setRange(prevRange);
        setDataRate(prevDataRate);
        setSamplesCount(prevSamplesCount);
        
        delay(100);
        
        if(logSerial && flag){ logSerial->println(F("HMC5883L: self-test was successful.\n")); }
        
        return flag;
    }
    
    vector3::Vector3 HMC5883L::read(){
        vector3::Vector3 norm, raw = (readRaw() * gain_) - bias_;

        
        norm.x = (calibrationMatrix_[0][0] * raw.x 
                        + calibrationMatrix_[0][1] * raw.y 
                            + calibrationMatrix_[0][2] * raw.z);
                            
        norm.y = (calibrationMatrix_[1][0] * raw.x 
                        + calibrationMatrix_[1][1] * raw.y 
                            + calibrationMatrix_[1][2] * raw.z);
                            
        norm.z = (calibrationMatrix_[2][0] * raw.x 
                        + calibrationMatrix_[2][1] * raw.y 
                            + calibrationMatrix_[2][2] * raw.z);
                            
        return norm;
    }
    
    void HMC5883L::setMode(uint8_t mode){
        i2c_utils::writeRegister8(addr_, regMode_, 
            (i2c_utils::readRegister8(addr_, regMode_) & 0xFC) | (Mode)mode);
    }
    
    void HMC5883L::setRange(uint8_t range){
        static const double gains[8] = {0.73f, 0.92f, 1.22f, 1.52f, 
                                       2.27f, 2.56f, 3.03f, 4.35f}; 
        
        i2c_utils::writeRegister8(addr_, regConfigB_, (Range)range << 5);
        gain_ = gains[(Range)range];
    }
    
    void HMC5883L::setDataRate(uint8_t dataRate){
        i2c_utils::writeRegister8(addr_, regConfigA_, 
            (i2c_utils::readRegister8(addr_, regConfigA_) & 0x63) | ((DataRate)dataRate << 2));
    }
    
    void HMC5883L::setSamplesCount(SamplesCount samplesCount){
        i2c_utils::writeRegister8(addr_, regConfigA_, 
            (i2c_utils::readRegister8(addr_, regConfigA_) & 0x1F) | (samplesCount << 5));
    }
    
    void HMC5883L::setBias(double bx, double by, double bz){
        bias_ = vector3::Vector3(bx, by, bz);
    }
    
    void HMC5883L::setCalibrationMatrix(double m00, double m01, double m02, 
                                        double m10, double m11, double m12, 
                                        double m20, double m21, double m22){
        calibrationMatrix_[0][0] = m00;
        calibrationMatrix_[0][1] = m01;
        calibrationMatrix_[0][2] = m02;
        calibrationMatrix_[1][0] = m10;
        calibrationMatrix_[1][1] = m11;
        calibrationMatrix_[1][2] = m12;
        calibrationMatrix_[2][0] = m20;
        calibrationMatrix_[2][1] = m21;
        calibrationMatrix_[2][2] = m22;
    }
    
    uint8_t HMC5883L::getMode(){
        return (Mode)(i2c_utils::readRegister8(addr_, regMode_) & 0x03);
    }
    
    uint8_t HMC5883L::getRange(){
        return (Range)((i2c_utils::readRegister8(addr_, regConfigB_) & 0xE0) >> 5);
    }
    
    uint8_t HMC5883L::getDataRate(){
        return (DataRate)((i2c_utils::readRegister8(addr_, regConfigA_) & 0x1C) >> 2);
    }
    
    SamplesCount HMC5883L::getSamplesCount(){
        return (SamplesCount)((i2c_utils::readRegister8(addr_, regConfigA_) & 0x60) >> 5);
    }
    
    vector3::Vector3 HMC5883L::readRaw(){
        uint8_t buff[6];
        i2c_utils::readNearRegisters(addr_, regOutXh_, buff, 0, 6);
        
        vector3::Vector3 raw;
        raw.x = (double)(buff[0] << 8 | buff[1]);
        raw.y = (double)(buff[4] << 8 | buff[5]);
        raw.z = (double)(buff[2] << 8 | buff[3]);
        
        return raw;
    }
    
    bool HMC5883L::selfTestCheckEachRange(bool isPositive, Stream* logSerial = NULL){
        const uint8_t avgSamplesCount = 100;
        
        static const int16_t lowerThreshold[8] = { 854,  679,  511, 411, 274, 243, 206, 143};
        static const int16_t upperThreshold[8] = {2020, 1607, 1209, 973, 649, 575, 487, 339};
        
        for(uint8_t range = RANGE_2_5GA; range <= RANGE_8_1GA; range++){
            if(logSerial){ logSerial->print(F("HMC5883L:        range: ")); logSerial->println(range); }
            
            setRange(range);
            delay(100);
            
            vector3::Vector3 val = {0.0f, 0.0f, 0.0f};
            for(uint8_t sampl = 0; sampl < avgSamplesCount; sampl++){
                val += readRaw();
            }
            val /= avgSamplesCount;

            if(logSerial){ logSerial->print(F("HMC5883L:        ")); logSerial->print(val.x);
                                                                     logSerial->print(F(" "));
                                                                     logSerial->print(val.y);
                                                                     logSerial->print(F(" "));
                                                                     logSerial->print(val.z);}           
            
            if(!isPositive){
                val = -val;
            }
            
            if(!val.isEachAxisInRange(lowerThreshold[range], upperThreshold[range])){
                if(logSerial){ logSerial->println(F(" ERROR")); }
                
                return false;
            }
            
            if(logSerial){ logSerial->println(F(" OK")); }
        }
        
        return true;
    }
}
}
}
