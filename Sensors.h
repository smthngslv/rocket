#ifndef SENSORS_H
#define SENSORS_H

#include <Arduino.h>

#include "ADXL345.h"
#include "L3G4200D.h"
#include "HMC5883L.h"
#include "BMP085.h"

#include "Quaternion.h"
#include "Vector3.h"

namespace rocket{
namespace sensors{
    class SensorsManager{
        public:
        bool initialize(Stream* logSerial = NULL);
        bool selfTest(Stream* logSerial = NULL);
        
        void update();
        void update(uint32_t lastUpdateTime);
        
        void magnetometerCallibration(Stream* logSerial = NULL);
        
                        double getFreq();
                        
                        double getEngineAcceleration();
              vector3::Vector3 getLinearAcceleration();
        quaternion::Quaternion getOrientation();
        
                       int16_t getAltitude();
                       int32_t getPressure();
                       int16_t getTemperature();
        
        
        
        static const double gConst_ = 9.8150f;
        
        static const double madgwickGainBeta_ = 0.125f;
        
        static const double engineAccelerationAlpha_ = 0.05f;
        static const double linearAccelerationAlpha_ = 0.125f;
        
        private:
        void madgwickAHRSupdate(vector3::Vector3 gyroMnt, 
                                vector3::Vector3 accelMnt, 
                                vector3::Vector3 magMnt);
                                
        void madgwickAHRSupdateIMU(vector3::Vector3 gyroMnt, 
                                   vector3::Vector3 accelMnt);
        
        //Время с последнего обновления.
        uint32_t lastUpdateTime_ = 0;
        
        //Частота опроса датчиков.
        double freq_ = 0;
        
        
        l3g4200d::L3G4200D gyroscope_;
        adxl345::ADXL345 accelerometer_;
        hmc5883l::HMC5883L magnetometer_;
        bmp085::BMP085 barometer_;
        
        //Ускорение двигателей.
        double engineAcceleration_ = 0.0f;
        
        //Вектор линейного ускорения.
        vector3::Vector3 linearAcceleration_ = {0.0f, 0.0f, 0.0f};
        
        //Ориентация.
        quaternion::Quaternion orientationQ_ = {1.0f, {0.0f, 0.0f, 0.0f}};
    };
}
}

#endif