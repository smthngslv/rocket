#include "Sensors.h"

#include <Arduino.h>
#include <avr/pgmspace.h>

#include "ADXL345.h"
#include "L3G4200D.h"
#include "HMC5883L.h"
#include "BMP085.h"

#include "Quaternion.h"
#include "Vector3.h"

namespace rocket{
namespace sensors{
    bool SensorsManager::initialize(Stream* logSerial = NULL){
        if(logSerial){ logSerial->println(F("Sensors: initialization...")); }
        
        //Гироскоп
        if(logSerial){ logSerial->print(F("Sensors: gyroscope, L3G4200D: ")); }
        if(!gyroscope_.initialize(l3g4200d::MODE_BYPASS, l3g4200d::RANGE_2000DPS, l3g4200d::DATARATE_800HZ)){
            if(logSerial){ logSerial->println(F("ERROR")); }
            return false;
        }
        
        gyroscope_.confLowPassFilter(l3g4200d::LPF_CUT_OFF_MIN);
        gyroscope_.enableLowPassFilter();
        
        gyroscope_.confThreshold();
        gyroscope_.enableThreshold();
        
        if(logSerial){ logSerial->println(F("OK")); }
        
        delay(100);
        
        //Акселерометр
        if(logSerial){ logSerial->print(F("Sensors: accelerometer, ADXL345: ")); }
        if(!accelerometer_.initialize(adxl345::MODE_BYPASS, adxl345::RANGE_4G, adxl345::DATARATE_800HZ)){
            if(logSerial){ logSerial->println(F("ERROR")); }
            return false;
        }
        
        //accelerometer_.setAxesGainCorrection(1.0000000f, 0.9500000f, 0.9023880f, 
                                             //0.9797001f, 1.0101734f, 1.2179073f);
        
        if(logSerial){ logSerial->println(F("OK")); }
        
        delay(100);
        
        //Магнитометр
        if(logSerial){ logSerial->print(F("Sensors: magnetometer, HMC5883L: ")); }
        if(!magnetometer_.initialize(hmc5883l::MODE_CONTINOUS, hmc5883l::RANGE_4GA, hmc5883l::DATARATE_75HZ)){
            if(logSerial){ logSerial->println(F("ERROR")); }
            return false;
        }

        magnetometer_.setBias(-21.56,-49.94,19.30);
        magnetometer_.setCalibrationMatrix(1.0f, 0.0f, 0.0f, 
                                           0.0f, 1.0f, 0.0f, 
                                           0.0f, 0.0f, 1.0f);
        
        magnetometer_.setSamplesCount(hmc5883l::SAMPLES_COUNT_8);
        
        if(logSerial){ logSerial->println(F("OK")); }
        
        delay(100);
        
        //Барометр
        if(logSerial){ logSerial->print(F("Sensors: barometer, BMP085: ")); }
        if(!barometer_.initialize(bmp085::OVERSAMPLING_ULTRA)){
            if(logSerial){ logSerial->println(F("ERROR")); }
            return false;
        }
        if(logSerial){ logSerial->println(F("OK")); }
        
        delay(100);
        
        if(logSerial){ logSerial->println(F("Sensors: initialization was successful. \n")); }
        
        return true;
    }
    
    bool SensorsManager::selfTest(Stream* logSerial = NULL){
        if(logSerial){ logSerial->println(F("Sensors: self-test...")); }
        
        //Гироскоп
        if(logSerial){ logSerial->println(F("Sensors: gyroscope, L3G4200D: ")); }
        bool flag = gyroscope_.selfTest(logSerial);
        delay(100);
        
        if(!flag){
            return false;
        }
            
        
        //Акселерометр
        if(logSerial){ logSerial->println(F("Sensors: accelerometer, ADXL345: ")); }
        flag = accelerometer_.selfTest(logSerial);
        delay(100);
        
        if(!flag){
            return false;
        }
            
        
        //Магнитометр
        if(logSerial){ logSerial->println(F("Sensors: magnetometer, HMC5883L: ")); }
        flag = magnetometer_.selfTest(logSerial);
        delay(100);
        
        //if(!flag){
        //    return false;
        //}
        
        //Барометр
        //if(logSerial){ logSerial->println(F("Sensors: barometer, ADXL345: ")); }
        //delay(100);
        
        
        if(logSerial && flag){ logSerial->println(F("Sensors: self-test was successful. \n")); }
        
        return flag;
    }
    
    void SensorsManager::update(){        
        //Запрашиваем обновление барометра.
        barometer_.requestUpdate();
        
        //Акселерометр.
        vector3::Vector3 accelMnt = accelerometer_.read();
        
        
        //Ускорение двигателей.
        engineAcceleration_ = engineAcceleration_ * (1.0f - engineAccelerationAlpha_) + 
                                        (accelMnt.length() - 1.0f) * engineAccelerationAlpha_;
        
        //Линейное ускорение.
        linearAcceleration_ = linearAcceleration_ * (1.0f - linearAccelerationAlpha_) + 
                                                    (orientationQ_.transform(accelMnt) - 
                                                        vector3::Vector3(0.0f, 0.0f, 1.0f)) * linearAccelerationAlpha_;
        
        //Ориентация.
        madgwickAHRSupdate(gyroscope_.readInRadians(), accelMnt, magnetometer_.read());
        
        
        //Вычисляем частоту.
        freq_ = 1000.0f / (millis() - lastUpdateTime_);
        
        lastUpdateTime_ = millis();
    }
    
    void SensorsManager::update(uint32_t lastUpdateTime){
        lastUpdateTime_ = lastUpdateTime;
        
        update();
    }
    
    
    void SensorsManager::magnetometerCallibration(Stream* logSerial = NULL){
        vector3::Vector3 bias = {0.0f, 0.0f, 0.0f};
        
        vector3::Vector3 mMin = {0.0f, 0.0f, 0.0f};
        vector3::Vector3 mMax = {0.0f, 0.0f, 0.0f};
        
        while(true){
            vector3::Vector3 magMnt = magnetometer_.read();
            
            if(mMin.x > magMnt.x){mMin.x = magMnt.x;}
            if(mMin.y > magMnt.y){mMin.y = magMnt.y;}
            if(mMin.z > magMnt.z){mMin.z = magMnt.z;}
            
            if(mMax.x < magMnt.x){mMax.x = magMnt.x;}
            if(mMax.y < magMnt.y){mMax.y = magMnt.y;}
            if(mMax.z < magMnt.z){mMax.z = magMnt.z;}
            
            bias.x = (mMin.x - mMax.x) / 2.0f - mMin.x;
            bias.y = (mMin.y - mMax.y) / 2.0f - mMin.y;
            bias.z = (mMin.z - mMax.z) / 2.0f - mMin.z;
            
            Serial.print(bias.x);  Serial.print(",");
            Serial.print(bias.y);  Serial.print(",");
            Serial.println(bias.z);
            
            delay(20);
        }
    }
    
    double SensorsManager::getFreq(){
        return freq_;
    }
    
    double SensorsManager::getEngineAcceleration(){
        return engineAcceleration_ * gConst_;
    }
    
    vector3::Vector3 SensorsManager::getLinearAcceleration(){
        return linearAcceleration_ * gConst_;
    }
    
    quaternion::Quaternion SensorsManager::getOrientation(){
        return orientationQ_;
    }
    
    int16_t SensorsManager::getAltitude(){
        return barometer_.readAltitude();
    }
    
    int32_t SensorsManager::getPressure(){
        return barometer_.readPressure();
    }
    
    int16_t SensorsManager::getTemperature(){
        return barometer_.readTemperature();
    }
    
    void SensorsManager::madgwickAHRSupdate(vector3::Vector3 gyroMnt, 
                                            vector3::Vector3 accelMnt, 
                                            vector3::Vector3 magMnt){
        
        if(((magMnt.x == 0.0f) && (magMnt.y == 0.0f) && (magMnt.z == 0.0f))){
            madgwickAHRSupdateIMU(gyroMnt, accelMnt);
            return;
        }
        
        double hx, hy;
        double _2q0mx, _2q0my, _2q0mz, _2q1mx, _2bx, _2bz, _4bx, _4bz, _2q0, _2q1, _2q2, _2q3, _2q0q2, _2q2q3, q0q0, q0q1, q0q2, q0q3, q1q1, q1q2, q1q3, q2q2, q2q3, q3q3;

        //Считаем гироскоп.
        quaternion::Quaternion gyroQ = (orientationQ_ * gyroMnt) * 0.5f;
        
        //Избегаем деления на 0.
        if(!((accelMnt.x == 0.0f) && (accelMnt.y == 0.0f) && (accelMnt.z == 0.0f))){
            magMnt.normalize();
            accelMnt.normalize();
            
            //Чтобы не считать одно и то же.
            _2q0 = 2.0f * orientationQ_.w;
            _2q1 = 2.0f * orientationQ_.v.x;
            _2q2 = 2.0f * orientationQ_.v.y;
            _2q3 = 2.0f * orientationQ_.v.z;
            
            _2q0mx = 2.0f * orientationQ_.w   * magMnt.x;
            _2q0my = 2.0f * orientationQ_.w   * magMnt.y;
            _2q0mz = 2.0f * orientationQ_.w   * magMnt.z;
            _2q1mx = 2.0f * orientationQ_.v.x * magMnt.x;
            
            _2q0q2 = 2.0f * orientationQ_.w   * orientationQ_.v.y;
            _2q2q3 = 2.0f * orientationQ_.v.y * orientationQ_.v.z;
            
            q0q0 = orientationQ_.w   * orientationQ_.w;
            q0q1 = orientationQ_.w   * orientationQ_.v.x;
            q0q2 = orientationQ_.w   * orientationQ_.v.y;
            q0q3 = orientationQ_.w   * orientationQ_.v.z;
            q1q1 = orientationQ_.v.x * orientationQ_.v.x;
            q1q2 = orientationQ_.v.x * orientationQ_.v.y;
            q1q3 = orientationQ_.v.x * orientationQ_.v.z;
            q2q2 = orientationQ_.v.y * orientationQ_.v.y;
            q2q3 = orientationQ_.v.y * orientationQ_.v.z;
            q3q3 = orientationQ_.v.z * orientationQ_.v.z;

            //Reference direction of Earth's magnetic field
            hx = magMnt.x * q0q0 - _2q0my * orientationQ_.v.z + _2q0mz * orientationQ_.v.y + magMnt.x * q1q1 + _2q1 * magMnt.y * orientationQ_.v.y + _2q1 * magMnt.z * orientationQ_.v.z - magMnt.x * q2q2 - magMnt.x * q3q3;
            hy = _2q0mx * orientationQ_.v.z + magMnt.y * q0q0 - _2q0mz * orientationQ_.v.x + _2q1mx * orientationQ_.v.y - magMnt.y * q1q1 + magMnt.y * q2q2 + _2q2 * magMnt.z * orientationQ_.v.z - magMnt.y * q3q3;
           
            _2bx = sqrt(hx * hx + hy * hy);
            _2bz = -_2q0mx * orientationQ_.v.y + _2q0my * orientationQ_.v.x + magMnt.z * q0q0 + _2q1mx * orientationQ_.v.z - magMnt.z * q1q1 + _2q2 * magMnt.y * orientationQ_.v.z - magMnt.z * q2q2 + magMnt.z * q3q3;
            _4bx = 2.0f * _2bx;
            _4bz = 2.0f * _2bz;
            
            //Gradient decent algorithm corrective step
            quaternion::Quaternion sQ;
            sQ.w   = -_2q2 * (2.0f * q1q3 - _2q0q2 - accelMnt.x) + _2q1 * (2.0f * q0q1 + _2q2q3 - accelMnt.y) - _2bz * orientationQ_.v.y * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - magMnt.x) + (-_2bx * orientationQ_.v.z + _2bz * orientationQ_.v.x) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - magMnt.y) + _2bx * orientationQ_.v.y * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - magMnt.z);
            sQ.v.x =  _2q3 * (2.0f * q1q3 - _2q0q2 - accelMnt.x) + _2q0 * (2.0f * q0q1 + _2q2q3 - accelMnt.y) - 4.0f * orientationQ_.v.x * (1 - 2.0f * q1q1 - 2.0f * q2q2 - accelMnt.z) + _2bz * orientationQ_.v.z * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - magMnt.x) + (_2bx * orientationQ_.v.y + _2bz * orientationQ_.w) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - magMnt.y) + (_2bx * orientationQ_.v.z - _4bz * orientationQ_.v.x) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - magMnt.z);
            sQ.v.y = -_2q0 * (2.0f * q1q3 - _2q0q2 - accelMnt.x) + _2q3 * (2.0f * q0q1 + _2q2q3 - accelMnt.y) - 4.0f * orientationQ_.v.y * (1 - 2.0f * q1q1 - 2.0f * q2q2 - accelMnt.z) + (-_4bx * orientationQ_.v.y - _2bz * orientationQ_.w) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - magMnt.x) + (_2bx * orientationQ_.v.x + _2bz * orientationQ_.v.z) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - magMnt.y) + (_2bx * orientationQ_.w - _4bz * orientationQ_.v.y) * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - magMnt.z);
            sQ.v.z =  _2q1 * (2.0f * q1q3 - _2q0q2 - accelMnt.x) + _2q2 * (2.0f * q0q1 + _2q2q3 - accelMnt.y) + (-_4bx * orientationQ_.v.z + _2bz * orientationQ_.v.x) * (_2bx * (0.5f - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - magMnt.x) + (-_2bx * orientationQ_.w + _2bz * orientationQ_.v.y) * (_2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - magMnt.y) + _2bx * orientationQ_.v.x * (_2bx * (q0q2 + q1q3) + _2bz * (0.5f - q1q1 - q2q2) - magMnt.z);
            
            sQ.normalize();
            
            //Apply feedback step
            gyroQ -= sQ * madgwickGainBeta_;
        }
        
        // Integrate rate of change of quaternion to yield quaternion
        orientationQ_ += gyroQ * ((millis() - lastUpdateTime_) / 1000.0f);
        
        orientationQ_.normalize();
    }
    
    void SensorsManager::madgwickAHRSupdateIMU(vector3::Vector3 gyroMnt, vector3::Vector3 accelMnt){
        double _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

        //Считаем гироскоп.
        quaternion::Quaternion gyroQ = (orientationQ_ * gyroMnt) * 0.5f;

        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if(!((accelMnt.x == 0.0f) && (accelMnt.y == 0.0f) && (accelMnt.z == 0.0f))) {
            accelMnt.normalize();  

            // Auxiliary variables to avoid repeated arithmetic
            _2q0 = 2.0f * orientationQ_.w;
            _2q1 = 2.0f * orientationQ_.v.x;
            _2q2 = 2.0f * orientationQ_.v.y;
            _2q3 = 2.0f * orientationQ_.v.z;
            _4q0 = 4.0f * orientationQ_.w;
            _4q1 = 4.0f * orientationQ_.v.x;
            _4q2 = 4.0f * orientationQ_.v.y;
            _8q1 = 8.0f * orientationQ_.v.x;
            _8q2 = 8.0f * orientationQ_.v.y;
            
            q0q0 = orientationQ_.w * orientationQ_.w;
            q1q1 = orientationQ_.v.x * orientationQ_.v.x;
            q2q2 = orientationQ_.v.y * orientationQ_.v.y;
            q3q3 = orientationQ_.v.z * orientationQ_.v.z;

            // Gradient decent algorithm corrective step
            quaternion::Quaternion sQ;
            sQ.w   = _4q0 * q2q2 + _2q2 * accelMnt.x + _4q0 * q1q1 - _2q1 * accelMnt.y;
            sQ.v.x = _4q1 * q3q3 - _2q3 * accelMnt.x + 4.0f * q0q0 * orientationQ_.v.x - _2q0 * accelMnt.y - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * accelMnt.z;
            sQ.v.y = 4.0f * q0q0 * orientationQ_.v.y + _2q0 * accelMnt.x + _4q2 * q3q3 - _2q3 * accelMnt.y - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * accelMnt.z;
            sQ.v.z = 4.0f * q1q1 * orientationQ_.v.z - _2q1 * accelMnt.x + 4.0f * q2q2 * orientationQ_.v.z - _2q2 * accelMnt.y;
            
            sQ.normalize();

            //Apply feedback step
            gyroQ -= sQ * madgwickGainBeta_;
        }
        
        // Integrate rate of change of quaternion to yield quaternion
        orientationQ_ += gyroQ * ((millis() - lastUpdateTime_) / 1000.0f);
        
        orientationQ_.normalize();
    }
}
}