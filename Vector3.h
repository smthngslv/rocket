#ifndef VECTOR3_H
#define VECTOR3_H

#include <Arduino.h>

namespace vector3{
    struct Vector3{    
        public:
        Vector3();
        Vector3(double x, double y, double z);
        
        Vector3 operator +  ();
        Vector3 operator -  ();
        
        Vector3 operator +  (const Vector3 &right);
        Vector3 operator -  (const Vector3 &right);
        Vector3 operator += (const Vector3 &right);
        Vector3 operator -= (const Vector3 &right);
        
        Vector3 operator *  (double right);
        Vector3 operator /  (double right);
        Vector3 operator *= (double right);
        Vector3 operator /= (double right);
        
        Vector3  normalize();
        
        double length();
        
        bool   isEachAxisInRange(double from, double to);
            
        double x;
        double y;
        double z;
    };
}

#endif