#include "Rocket.h"

#include <Arduino.h>
#include <Stream.h>

#include "Secure.h"
#include "Sensors.h"
#include "Quaternion.h"

namespace rocket{
    RocketManager::RocketManager(){
        
    }
    
    bool RocketManager::initialize(Stream* controlSerial){
        controlSerial_ = secure::SecureSerial(controlSerial, this);
        
        if(!sensorsManager_.initialize(&controlSerial_)){
            controlSerial_.flush();
            
            return false;
        }
        controlSerial_.flush();
        
        delay(1000);
        
        if(!sensorsManager_.selfTest(&controlSerial_)){
            controlSerial_.flush();
            
            return false;
        }
        controlSerial_.flush();
        
        //sensorsManager_.magnetometerCallibration();
        
        //Устанавливаем время последнего обновления.
        sensorsManager_.update(millis());
        
        return true;
    }
    
    void RocketManager::update(){
        //Обновляем датчики.
        sensorsManager_.update();
        
        //Ориентация.
        lastSensorsData_.qOrient = sensorsManager_.getOrientation();
        
        lastSensorsData_.pressure    = sensorsManager_.getPressure();
        lastSensorsData_.temperature = sensorsManager_.getTemperature();
        
        
        //Логика ракеты.
        
        
        //Связь.
        controlSerial_.update();
        
        //controlSerial_.sendDataGramm(UnionSensorsData(lastSensorsData_).rawData, (uint8_t)sizeof(lastSensorsData_), (uint8_t)TYPE_RESPONCE_SENSORS_DATA);
        
        //Serial.println(sensorsManager_.getFreq());
        
    }
    
    void recvDataGrammCallBack(uint8_t* buff, uint8_t length, uint8_t type, bool verify){
        
    }
}