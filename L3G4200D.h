#ifndef L3G4200D_H
#define L3G4200D_H

#include <Arduino.h>
#include <Stream.h>

#include "ISensorIMU.h"
#include "Vector3.h"

namespace rocket{
namespace sensors{
namespace l3g4200d{
    enum Mode: uint8_t{
        MODE_STREAM = 0b10,
        MODE_FIFO   = 0b01,
        MODE_BYPASS = 0b00
    };
    
    enum Range: uint8_t{
        RANGE_2000DPS = 0b10,
        RANGE_500DPS  = 0b01,
        RANGE_250DPS  = 0b00
    };
    
    enum DataRate: uint8_t{
        DATARATE_800HZ = 0b11,
        DATARATE_400HZ = 0b10,
        DATARATE_200HZ = 0b01,
        DATARATE_100HZ = 0b00   
    };
    
    enum LPFilterCutOff: uint8_t{
        LPF_CUT_OFF_MAX    = 0b11,
        LPF_CUT_OFF_HIGH   = 0b10,
        LPF_CUT_OFF_MEDIUM = 0b01,
        LPF_CUT_OFF_MIN    = 0b00
    };
    
    class L3G4200D: virtual public ISensorIMU{
        public:
            virtual bool initialize(uint8_t mode, uint8_t range, uint8_t dataRate);
            virtual bool selfTest(Stream* logSerial = NULL);
            virtual vector3::Vector3 read();
            vector3::Vector3 readInRadians();
            
            void confThreshold();
            void confLowPassFilter(LPFilterCutOff lpFilterCutOff);
            
            void enableThreshold();
            void enableLowPassFilter();
            
            void disableThreshold();
            void disableLowPassFilter();
            
            virtual void setMode(uint8_t mode);
            virtual void setRange(uint8_t range);
            virtual void setDataRate(uint8_t dataRate);
            
            virtual uint8_t getMode();
            virtual uint8_t getRange();
            virtual uint8_t getDataRate();
            
        protected:
            virtual vector3::Vector3 readRaw();
            
            bool selfTestCheckEachRange(bool isPositive, Stream* logSerial = NULL);
            
            const uint8_t addr_        = 0x69;
            const uint8_t regWhoAmI_   = 0x0F;
            
            const uint8_t regCtrl_1_   = 0x20;
            const uint8_t regCtrl_4_   = 0x23;
            const uint8_t regCtrl_5_   = 0x24;
            const uint8_t regFifoCtrl_ = 0x2E;
            
            const uint8_t regOutXl_    = 0x28;
            
            bool useThreshold_ = false;
            
            double gain_;
            
            vector3::Vector3 threshold_ = {0.0f, 0.0f, 0.0f};
    };
}
}
}

#endif