#include "Vector3.h"
#include <Arduino.h>

namespace vector3{
    Vector3::Vector3(){
        x = 0;
        y = 0;
        z = 0;
    }
    
    Vector3::Vector3(double x, double y, double z){
        this->x = x;
        this->y = y;
        this->z = z;
    }
    
    Vector3 Vector3::operator +  (){
        return *this;
    }        
    
    Vector3 Vector3::operator -  (){
        return Vector3(-x, -y, -z);
    }
    
    
    Vector3 Vector3::operator +  (const Vector3 &right){
        return Vector3(x + right.x, y + right.y, z + right.z);
    }        
    
    Vector3 Vector3::operator -  (const Vector3 &right){
        return Vector3(x - right.x, y - right.y, z - right.z);
    }
    
    Vector3 Vector3::operator += (const Vector3 &right){
        x += right.x;
        y += right.y;
        z += right.z;
        
        return *this;
    }  
    
    Vector3 Vector3::operator -= (const Vector3 &right){
        x -= right.x;
        y -= right.y;
        z -= right.z;
        
        return *this;
    }  
    
    
    Vector3 Vector3::operator *  (double right){
        return Vector3(x * right, y * right, z * right);
    }        
    
    Vector3 Vector3::operator /  (double right){
        return Vector3(x / right, y / right, z / right);
    } 
    
    Vector3 Vector3::operator *= (double right){
        x *= right;
        y *= right;
        z *= right;
        
        return *this;
    }
    
    Vector3 Vector3::operator /= (double right){
        x /= right;
        y /= right;
        z /= right;
        
        return *this;
    }
    
    Vector3 Vector3::normalize(){
        double len = length();
        
        if(len > 0){
            x /= len;
            y /= len;
            z /= len;
        }
        
        return *this;
    }
    
    double  Vector3::length(){
        return sqrt(x * x + y * y + z * z);
    }
    
    bool Vector3::isEachAxisInRange(double from, double to){
        return ((x > from && x < to)
             && (y > from && y < to)
             && (z > from && z < to));
    }
}