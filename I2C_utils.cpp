#include "I2C_utils.h"

#include <Arduino.h>
#include <Wire.h>

namespace i2c_utils{
    void writeRegister8(uint8_t addr, uint8_t reg, uint8_t value){
        Wire.beginTransmission(addr);
        
        Wire.write(reg);
        Wire.write(value);
        
        Wire.endTransmission();
    }
    
    uint8_t readRegister8(uint8_t addr, uint8_t reg){
        Wire.beginTransmission(addr);
        Wire.write(reg);
        Wire.endTransmission();
        
        Wire.requestFrom(addr, (uint8_t)1);
        
        return Wire.read();
    }
    
    void readNearRegisters(uint8_t addr, uint8_t reg, uint8_t buff[], 
                           uint8_t buffStartIndex, uint8_t count){
        Wire.beginTransmission(addr);
        Wire.write(reg);
        Wire.endTransmission();
        
        Wire.requestFrom(addr, count);
        
        for(uint8_t i = 0; i < count; i++){
            buff[buffStartIndex + i] = Wire.read();
        }
    }
}