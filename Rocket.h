#ifndef ROCKET_H
#define ROCKET_H

#include <Arduino.h>
#include <Stream.h>

#include "Secure.h"
#include "Sensors.h"
#include "Quaternion.h"

namespace rocket{
    class RocketManager: public secure::ISecureSerialCallBacks{
        public:
        RocketManager();
        
        bool initialize(Stream* controlSerial);
        void update();

        private: 
        enum DataGrammTypes: uint8_t{
            TYPE_TEXT                  = 0x00,
            TYPE_REQUEST_SENSORS_DATA  = 0x01,
            TYPE_RESPONCE_SENSORS_DATA = 0x02,
        };
        
        struct SensorsData{
            //Ориентация.
            quaternion::Quaternion qOrient;
            
            int32_t pressure;
            int16_t temperature;

        };
        
        union UnionSensorsData{
            UnionSensorsData(SensorsData data_){ data = data_; }
            
            SensorsData data;
            uint8_t rawData[(uint8_t)sizeof(SensorsData)];
        };
        
        
        virtual void recvDataGrammCallBack(uint8_t* buff, uint8_t length, uint8_t type, bool verify);
        
        secure::SecureSerial controlSerial_;
        sensors::SensorsManager sensorsManager_;
        
        SensorsData lastSensorsData_;
        
        
    };
}

#endif