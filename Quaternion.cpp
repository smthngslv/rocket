#include "Quaternion.h"

#include <Arduino.h>
#include "Vector3.h"

namespace quaternion{
    Quaternion::Quaternion(){
    
    }
    
    Quaternion::Quaternion(double angle, vector3::Vector3 v){
        angle /= 2;
        
        this->w = cos(angle);
        this->v = v.normalize() * sin(angle);
    }
    
    Quaternion::Quaternion(double w, double x, double y, double z){
        this->w = w;
        this->v = vector3::Vector3(x, y, z);
    }
    
    Quaternion Quaternion::normalize(){
        double len = length();
        
        if(len > 0){
            w /= len;
            v /= len;
        }
        
        return *this;
    }
    
    double Quaternion::length(){
        return sqrt(w * w + v.x * v.x + v.y * v.y + v.z * v.z);
    }
    
    vector3::Vector3 Quaternion::transform(vector3::Vector3 mv){
        return ((Quaternion(w, v.x, v.y, v.z) * mv) * (- *this)).v;
    }
    
    Quaternion Quaternion::operator +  (){
        return *this;
    }        
    
    Quaternion Quaternion::operator -  (){
        return Quaternion(w, -v.x, -v.y, -v.z).normalize();
    }
    
    
    Quaternion Quaternion::operator +  (const Quaternion &right){
        return Quaternion(w + right.w, v.x + right.v.x, v.y + right.v.y, v.z + right.v.z);
    }
    
    Quaternion Quaternion::operator -  (const Quaternion &right){
        return Quaternion(w - right.w, v.x - right.v.x, v.y - right.v.y, v.z - right.v.z);
    }
    
    
    Quaternion Quaternion::operator += (const Quaternion &right){
        *this = Quaternion(w + right.w, v.x + right.v.x, v.y + right.v.y, v.z + right.v.z);
        
        return *this;
    }
    
    Quaternion Quaternion::operator -= (const Quaternion &right){
        *this = Quaternion(w - right.w, v.x - right.v.x, v.y - right.v.y, v.z - right.v.z);
        
        return *this;
    }
    
    
    Quaternion Quaternion::operator *  (double right){
        return Quaternion(w * right, v.x * right, v.y * right, v.z * right);
    }
    
    Quaternion Quaternion::operator *= (double right){
        *this = Quaternion(w * right, v.x * right, v.y * right, v.z * right);
        
        return *this;
    }
    
    
    Quaternion Quaternion::operator *  (const Quaternion &right){
        return Quaternion(w * right.w   - v.x * right.v.x - v.y * right.v.y - v.z * right.v.z,
                          w * right.v.x + v.x * right.w   + v.y * right.v.z - v.z * right.v.y,
                          w * right.v.y - v.x * right.v.z + v.y * right.w   + v.z * right.v.x,
                          w * right.v.z + v.x * right.v.y - v.y * right.v.x + v.z * right.w);
    }
    
    Quaternion Quaternion::operator *= (const Quaternion &right){
        *this = Quaternion(w * right.w   - v.x * right.v.x - v.y * right.v.y - v.z * right.v.z,
                           w * right.v.x + v.x * right.w   + v.y * right.v.z - v.z * right.v.y,
                           w * right.v.y - v.x * right.v.z + v.y * right.w   + v.z * right.v.x,
                           w * right.v.z + v.x * right.v.y - v.y * right.v.x + v.z * right.w);
                           
        return *this;
    }
    
    
    Quaternion Quaternion::operator *  (const vector3::Vector3 &right){
        return Quaternion(- v.x * right.x - v.y * right.y - v.z * right.z,
                              w * right.x + v.y * right.z - v.z * right.y,
                              w * right.y - v.x * right.z + v.z * right.x,
                              w * right.z + v.x * right.y - v.y * right.x);
        
    }
    
    Quaternion Quaternion::operator *= (const vector3::Vector3 &right){
        *this = Quaternion(- v.x * right.x - v.y * right.y - v.z * right.z,
                               w * right.x + v.y * right.z - v.z * right.y,
                               w * right.y - v.x * right.z + v.z * right.x,
                               w * right.z + v.x * right.y - v.y * right.x);
                               
        return *this;
    }
}