#include "BMP085.h"

#include <Arduino.h>
#include "I2C_utils.h"

namespace rocket{
namespace sensors{
namespace bmp085{
    bool BMP085::initialize(Oversampling oversampling){
        if (i2c_utils::readRegister8(addr_, regChipId_) != 0x55){
            return false;
        }

        //Данные калибровки.
        uint8_t buff[22];
        i2c_utils::readNearRegisters(addr_, regAc1_, buff, 0, 22);
        
        ac1_ =  (int16_t)(buff[0]  << 8 | buff[1]);
        ac2_ =  (int16_t)(buff[2]  << 8 | buff[3]);
        ac3_ =  (int16_t)(buff[4]  << 8 | buff[5]);
        ac4_ = (uint16_t)(buff[6]  << 8 | buff[7]);
        ac5_ = (uint16_t)(buff[8]  << 8 | buff[9]);
        ac6_ = (uint16_t)(buff[10] << 8 | buff[11]);
        
        b1_  =  (int16_t)(buff[12] << 8 | buff[13]);
        b2_  =  (int16_t)(buff[14] << 8 | buff[15]);
        
        mb_  =  (int16_t)(buff[16] << 8 | buff[17]);
        mc_  =  (int16_t)(buff[18] << 8 | buff[19]);
        md_  =  (int16_t)(buff[20] << 8 | buff[21]);
        
        oversampling_ = oversampling;           
        
        return true;
    }
    
    
    void BMP085::requestUpdate(){
        //Если не прошло достаточно времени.
        if(readyDataTime_ > millis()){
            return;
        }
        
        //Если была запрошена температура, то просим давление.
        if(isTemperatureRequested_){
            isTemperatureRequested_ = false;
            
            //Сохраняем
            lastRawTemperature_ = readRawTemperature();
            
            //Запрос на обновление давления.
            i2c_utils::writeRegister8(addr_, regCtrl_, 0x34 | (oversampling_ << 6));
            
            //Время.
            switch (oversampling_){
                case OVERSAMPLING_ULTRA:
                    readyDataTime_ = millis() + 26;
                    
                    break;
                case OVERSAMPLING_HIGH:
                    readyDataTime_ = millis() + 14;
                
                    break;
                case OVERSAMPLING_STANDARD:
                    readyDataTime_ = millis() + 8;
                
                    break;
                case OVERSAMPLING_OFF:
                    readyDataTime_ = millis() + 5;
                
                    break;
            }
            
            return;
        }
        
        
        //Расчёты.
        processRawValues(readRawPressure(), lastRawTemperature_);
    
        //Просим температуру.
        i2c_utils::writeRegister8(addr_, regCtrl_, 0x2E);
        
        //Время.
        readyDataTime_ = millis() + 5;
        
        //Флаг.
        isTemperatureRequested_ = true;   
    }
    
    int16_t BMP085::readAltitude(){
        return lastAltitude_;
    }
    
    int32_t BMP085::readPressure(){
        return lastPressure_;
    }
    
    int16_t BMP085::readTemperature(){
        return lastTemperature_;
    }
    
    void BMP085::setSeaLevelPressure(uint32_t seaLevelPressure){
        seaLevelPressure_ = seaLevelPressure;
    }
    
    void BMP085::processRawValues(int32_t rawPressure, uint16_t rawTemperature){
        int32_t  B3, B5, B6, X1, X2, X3;
        uint32_t B4, B7;

        //Температура.
        X1 = ((int32_t)rawTemperature - (int32_t)ac6_) * ((int32_t)ac5_) >> 15;
        X2 = ((int32_t)mc_ << 11) / (X1+(int32_t)md_);
        B5 = X1 + X2;
        
        lastTemperature_ = (B5 + 8) >> 4;
        
        //Давление.
        B6 = B5 - 4000;
        X1 = ((int32_t)b2_ * ( (B6 * B6)>>12 )) >> 11;
        X2 = ((int32_t)ac2_ * B6) >> 11;
        X3 = X1 + X2;
        B3 = ((((int32_t)ac1_ * 4 + X3) << oversampling_) + 2) / 4;

        X1 = ((int32_t)ac3_ * B6) >> 13;
        X2 = ((int32_t)b1_ * ((B6 * B6) >> 12)) >> 16;
        X3 = ((X1 + X2) + 2) >> 2;
        B4 = ((uint32_t)ac4_ * (uint32_t)(X3 + 32768)) >> 15;
        B7 = ((uint32_t)rawPressure - B3) * (uint32_t)( 50000UL >> oversampling_ );


        if (B7 < 0x80000000) {
            lastPressure_ = (B7 * 2) / B4;
        } 
        else{
            lastPressure_ = (B7 / B4) * 2;
        }
        
        X1 = (lastPressure_ >> 8) * (lastPressure_ >> 8);
        X1 = (X1 * 3038) >> 16;
        X2 = (-7357 * lastPressure_) >> 16;

        lastPressure_ += ((X1 + X2 + (int32_t)3791)>>4); 
        
        lastAltitude_ = 10 * 44330 * (1.0f - pow((double)lastPressure_ / (double)seaLevelPressure_, 0.1903f));    
        
        //Фильтр
        //lastAltitude_ = lastAltitude_ * 0.9 + (10 * 44330 * (1.0f - pow((double)lastPressure_ / (double)seaLevelPressure_, 0.1903f))) * 0.1;    
    }
    
    int32_t BMP085::readRawPressure(){
        uint8_t buff[3];
        i2c_utils::readNearRegisters(addr_, regData_,buff, 0, 3);
        
        int32_t rawPressure = (int32_t)(((int32_t)buff[0] << 16) | 
                                        ((int32_t)buff[1] << 8)  | 
                                         (int32_t)buff[2]);
        
        return rawPressure >> (8 - oversampling_);
    }
    
    uint16_t BMP085::readRawTemperature(){
        uint8_t buff[2];
        i2c_utils::readNearRegisters(addr_, regData_, buff, 0, 2);
        
        return (uint16_t)(buff[0] << 8 | buff[1]);
    }
}
}
} 