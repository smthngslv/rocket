#include "L3G4200D.h"

#include <Arduino.h>
#include <Stream.h>
#include <avr/pgmspace.h>

#include "Vector3.h"
#include "I2C_utils.h"

namespace rocket{
namespace sensors{
namespace l3g4200d{
    bool L3G4200D::initialize(uint8_t mode, uint8_t range, uint8_t dataRate){
        if(i2c_utils::readRegister8(addr_, regWhoAmI_) != 0xD3){
            return false;
        }
        
        //Включаем все оси, запрещаем выключаться.
        i2c_utils::writeRegister8(addr_, regCtrl_1_, 0x0F);
        
        //Запрещаем записывать значения, пока мы читаем.
        i2c_utils::writeRegister8(addr_, regCtrl_4_, 0x80);
        
        //Разрешаем FIFO.
        i2c_utils::writeRegister8(addr_, regCtrl_5_, 0x40);
        
        setMode(mode);
        setRange(range);
        setDataRate(dataRate);
        
        return true;
    }
    
    bool L3G4200D::selfTest(Stream* logSerial = NULL){
        if(logSerial){ logSerial->println(F("L3G4200D: self-test begining...")); }
        
        Mode prevMode = getMode();
        Range prevRange = getRange();
        DataRate prevDataRate = getDataRate();
          
        bool thresholdWasEnableFlag = useThreshold_;
        bool lowPassFilterWasEnableFlag = (bool)(i2c_utils::readRegister8(addr_, regCtrl_5_) & 0x03);
        
        disableThreshold();
        disableLowPassFilter();
        
        
        setMode(MODE_BYPASS);
        setDataRate(DATARATE_800HZ);
        
        if(logSerial){ logSerial->println(F("L3G4200D:    positive direction:")); }
        
        //+
        i2c_utils::writeRegister8(addr_, regCtrl_4_, 
            (i2c_utils::readRegister8(addr_, regCtrl_4_) & 0xF9) | (0x01 << 1));
            
        bool flag = selfTestCheckEachRange(true, logSerial);
        
        if(flag){
            if(logSerial){ logSerial->println(F("L3G4200D:    negative direction:")); }
            
            //-
            i2c_utils::writeRegister8(addr_, regCtrl_4_, 
                (i2c_utils::readRegister8(addr_, regCtrl_4_) & 0xF9) | (0x03 << 1));
                
            flag = selfTestCheckEachRange(false, logSerial);
        }
        
        //Выключаем.
        i2c_utils::writeRegister8(addr_, regCtrl_4_, 
            (i2c_utils::readRegister8(addr_, regCtrl_4_) & 0xF9));
        
        setMode(prevMode);
        setRange(prevRange);
        setDataRate(prevDataRate);
        
        if(thresholdWasEnableFlag){
            enableThreshold();
        }
        
        if(lowPassFilterWasEnableFlag){
            enableLowPassFilter();
        }
        
        delay(100);
        
        if(logSerial && flag){ logSerial->println(F("L3G4200D: self-test was successful.\n")); }
        
        return flag;
    }
    
    vector3::Vector3 L3G4200D::read(){
        vector3::Vector3 normDeg = readRaw() * gain_;
       
        if(!useThreshold_){
            return normDeg;
        }
       
        if(threshold_.x > abs(normDeg.x)){
            normDeg.x = 0.0f;
        }
        
        if(threshold_.y > abs(normDeg.y)){
            normDeg.y = 0.0f;
        }
        
        if(threshold_.z > abs(normDeg.z)){
            normDeg.z = 0.0f;
        }
        
        return normDeg;
    }
    
    vector3::Vector3 L3G4200D::readInRadians(){
        return read() * 0.0174532f;
    }
    
    void L3G4200D::confThreshold(){
        const int16_t avgSamplesCount = 5000;
        
        vector3::Vector3 tmpThreshold = {0.0f, 0.0f, 0.0f};
        
        for(int16_t smpl = 0; smpl < avgSamplesCount; smpl++){
            vector3::Vector3 norm = read();
            
            tmpThreshold.x += abs(norm.x);
            tmpThreshold.y += abs(norm.y);
            tmpThreshold.z += abs(norm.z);         
        }
        
        threshold_ = (tmpThreshold / avgSamplesCount) * 1.5f;
        
        useThreshold_ = true;
    }
    
    void L3G4200D::confLowPassFilter(LPFilterCutOff lpFilterCutOff){
        i2c_utils::writeRegister8(addr_, regCtrl_1_, 
            (i2c_utils::readRegister8(addr_, regCtrl_1_) & 0xCF) | lpFilterCutOff << 4);
    }
    
    void L3G4200D::enableThreshold(){
        useThreshold_ = true;
    }
    
    void L3G4200D::enableLowPassFilter(){
        i2c_utils::writeRegister8(addr_, regCtrl_5_, 
            (i2c_utils::readRegister8(addr_, regCtrl_5_) & 0xFC) | 0x02);
    }
    
    void L3G4200D::disableThreshold(){
        useThreshold_ = false;
    }
    
    void L3G4200D::disableLowPassFilter(){
        i2c_utils::writeRegister8(addr_, regCtrl_5_, 
            (i2c_utils::readRegister8(addr_, regCtrl_5_) & 0xFC));
    }
    
    void L3G4200D::setMode(uint8_t mode){
        i2c_utils::writeRegister8(addr_, regFifoCtrl_, 
            (i2c_utils::readRegister8(addr_, regFifoCtrl_) & 0x1F) | ((Mode)mode << 5));
    }
    
    void L3G4200D::setRange(uint8_t range){
        static const double gains[3] = {0.00875f, 0.0175f, 0.07f};
        
        i2c_utils::writeRegister8(addr_, regCtrl_4_, 
            (i2c_utils::readRegister8(addr_, regCtrl_4_) & 0xCF) | ((Range)range << 4));
        gain_ = gains[(Range)range];  
    }
    
    void L3G4200D::setDataRate(uint8_t dataRate){
        i2c_utils::writeRegister8(addr_, regCtrl_1_, 
            (i2c_utils::readRegister8(addr_, regCtrl_1_) & 0x3F) | ((DataRate)dataRate << 6));
    }
    
    uint8_t L3G4200D::getMode(){
        return (Mode)((i2c_utils::readRegister8(addr_, regFifoCtrl_) & 0x60) >> 5);
    }
    
    uint8_t L3G4200D::getRange(){
        return (Range)((i2c_utils::readRegister8(addr_, regCtrl_4_) & 0x30) >> 4);
    }
    
    uint8_t L3G4200D::getDataRate(){
        return (DataRate)((i2c_utils::readRegister8(addr_, regCtrl_1_) & 0xC0) >> 6);
    }
    
    vector3::Vector3 L3G4200D::readRaw(){
        uint8_t buff[6];
        
        // Чтобы прочитать несколько байт сразу.
        i2c_utils::readNearRegisters(addr_, regOutXl_ | (1 << 7), buff, 0, 6);
        
        vector3::Vector3 raw;
        raw.x = (double)(buff[1] << 8 | buff[0]);
        raw.y = (double)(buff[3] << 8 | buff[2]);
        raw.z = (double)(buff[5] << 8 | buff[4]);
        
        return raw;
    }
    
    bool L3G4200D::selfTestCheckEachRange(bool isPositive, Stream* logSerial = NULL){
        const uint8_t avgSamplesCount = 100;
        
        static const int16_t lowerThreshold[3] = {110, 170, 430};
        static const int16_t upperThreshold[3] = {150, 230, 610};
        
        for(uint8_t range = RANGE_250DPS; range <= RANGE_2000DPS; range++){
            if(logSerial){ logSerial->print(F("L3G4200D:        range: ")); logSerial->println(range); }
            
            setRange(range);
            delay(100);
            
            vector3::Vector3 val = {0.0f, 0.0f, 0.0f};
            for(uint8_t sampl = 0; sampl < avgSamplesCount; sampl++){
                val += read();
            }
            val /= avgSamplesCount;
            
            //Приводим к одному знаку.
            val.x *= -1;
            val.z *= -1;
            
            if(logSerial){ logSerial->print(F("L3G4200D:        ")); logSerial->print(val.x);
                                                                     logSerial->print(F(" "));
                                                                     logSerial->print(val.y);
                                                                     logSerial->print(F(" "));
                                                                     logSerial->print(val.z);}
            
            if(!isPositive){
                val = -val;
            }
            
            if(!val.isEachAxisInRange(lowerThreshold[range], upperThreshold[range])){
                if(logSerial){ logSerial->println(F(" ERROR")); }
                
                return false;
            }
            
            if(logSerial){ logSerial->println(F(" OK")); }
        }
        
        return true;
    }
}
}
}