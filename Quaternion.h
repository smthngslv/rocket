#ifndef QUATERNION_H
#define QUATERNION_H

#include <Arduino.h>
#include "Vector3.h"

namespace quaternion{
    struct Quaternion{
        public:
        Quaternion();
        Quaternion(double w, vector3::Vector3 v);
        Quaternion(double w, double x, double y, double z);
        
        Quaternion normalize();
        
        double length();
        
        vector3::Vector3 transform(vector3::Vector3 mv);
        
        Quaternion operator +  ();
        Quaternion operator -  ();
        
        Quaternion operator +  (const Quaternion &right);
        Quaternion operator -  (const Quaternion &right);
        
        Quaternion operator += (const Quaternion &right);
        Quaternion operator -= (const Quaternion &right);
        
        
        Quaternion operator *  (double right);
        Quaternion operator *= (double right);
        
        Quaternion operator *  (const Quaternion &right);
        Quaternion operator *= (const Quaternion &right);
        
        Quaternion operator *  (const vector3::Vector3 &right);
        Quaternion operator *= (const vector3::Vector3 &right);
        
        double w;
        vector3::Vector3 v;
    };
}

#endif