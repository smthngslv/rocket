#ifndef I2C_UTILS_H
#define I2C_UTILS_H

#include <Arduino.h>
#include <Wire.h>

namespace i2c_utils{
    //Записывает value в 8 битный регистр reg устройства addr.
    void writeRegister8(uint8_t addr, uint8_t reg, uint8_t value);
    //Читает 8 битный регистр reg устройства addr.
    uint8_t readRegister8(uint8_t addr, uint8_t reg);
    //Читает последовательно count 8 битных регистров, начиная с reg устройства addr;
    void readNearRegisters(uint8_t addr, uint8_t reg, uint8_t buff[], uint8_t buffStartIndex, uint8_t count);
}

#endif