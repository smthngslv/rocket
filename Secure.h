#ifndef SECURESERIAL_H
#define SECURESERIAL_H

#include <Arduino.h>
#include <Stream.h>

namespace rocket{
namespace secure{
    class ISecureSerialCallBacks{
        public:
        virtual void recvDataGrammCallBack(uint8_t* buff, uint8_t length, uint8_t type, bool verify) = 0;
    };
    
    class SecureSerial: public Stream{
        public:
        static const uint8_t bufferSize_ = 128;
        
        SecureSerial();
        SecureSerial(Stream* internalSerial, ISecureSerialCallBacks* secureSerialCallBacks);
        
        ~SecureSerial();
        
        //Наследуемые от Stream, пустые.
        virtual int16_t read()           override;
        virtual int16_t peek()           override;
        virtual int16_t available()      override;
        
        //Что-то делают.
        virtual  size_t write(uint8_t b) override;
        virtual    void flush()          override;
        
        //Новые возможности.
        void sendDataGramm(uint8_t* buff, uint8_t length, uint8_t type);
        
        //Вызывается в цикле, принимает данные.
        void update();
        
        protected:
        void sendPacket(uint8_t* buff, uint8_t length, uint8_t type);
        
        uint16_t getCRC(uint8_t* buff, uint8_t length, uint8_t type);
        
        uint8_t sendBuffer_[bufferSize_];
        uint8_t recvBuffer_[bufferSize_];
        
        uint8_t sendBufferPos_ = 0;
        uint8_t recvBufferPos_ = 0;
                
        
        uint8_t recvPacketLen_;
        uint8_t recvPacketType_;
        uint16_t recvPacketHash_;
        
        uint8_t recvPacketState_ = 0;
        
        Stream* internalSerial_;
        
        ISecureSerialCallBacks* secureSerialCallBacks_;
    };
}
}

#endif