#ifndef ADXL345_H
#define ADXL345_H

#include <Arduino.h>
#include <Stream.h>

#include "ISensorIMU.h"
#include "Vector3.h"

namespace rocket{
namespace sensors{
namespace adxl345{
    enum Mode: uint8_t{
        MODE_STREAM = 0b10,
        MODE_FIFO   = 0b01,
        MODE_BYPASS = 0b00
    };
    
    enum Range: uint8_t{
        RANGE_16G = 0b11,
        RANGE_8G  = 0b10,
        RANGE_4G  = 0b01,
        RANGE_2G  = 0b00
    };
    
    enum DataRate: uint8_t{
        DATARATE_3200HZ = 0b1111,
        DATARATE_1600HZ = 0b1110,
        DATARATE_800HZ  = 0b1101,
        DATARATE_400HZ  = 0b1100,
        DATARATE_200HZ  = 0b1011,
        DATARATE_100HZ  = 0b1010,
        DATARATE_50HZ   = 0b1001,
        DATARATE_25HZ   = 0b1000,
        DATARATE_12_5HZ = 0b0111,
        DATARATE_6_25HZ = 0b0110,
        DATARATE_3_13HZ = 0b0101,
        DATARATE_1_56HZ = 0b0100,
        DATARATE_0_78HZ = 0b0011,
        DATARATE_0_39HZ = 0b0010,
        DATARATE_0_20HZ = 0b0001,
        DATARATE_0_10HZ = 0b0000
    };
    
    class ADXL345: virtual public ISensorIMU{
        public:
        virtual bool initialize(uint8_t mode, uint8_t range, uint8_t dataRate);
        virtual bool selfTest(Stream* logSerial = NULL);
        virtual vector3::Vector3 read();
        
        virtual void setMode(uint8_t mode);
        virtual void setRange(uint8_t range);
        virtual void setDataRate(uint8_t dataRate);
                void setAxesGainCorrection(double cx, double cy, double cz, 
                                           double cxn, double cyn, double czn);
                
        virtual uint8_t getMode();
        virtual uint8_t getRange();
        virtual uint8_t getDataRate();
            
        protected:
        virtual vector3::Vector3 readRaw();
        
        bool selfTestCheckEachRange(Stream* logSerial = NULL);
        
        const uint8_t addr_          = 0x53;
        const uint8_t regDevId_      = 0x00;
        
        const uint8_t regBWRate_     = 0x2C;
        const uint8_t regPowerCtrl_  = 0x2D;
        const uint8_t regDataFormat_ = 0x31;
        const uint8_t regFifoCtrl_   = 0x38;
        
        const uint8_t regOutXl_      = 0x32;
        
        double gain_;
        vector3::Vector3 axesGainCorrection_ = {1.0f, 1.0f, 1.0f};
        vector3::Vector3 axesGainNegativeCorrection_ = {1.0f, 1.0f, 1.0f};
            
    };
}
}
}

#endif